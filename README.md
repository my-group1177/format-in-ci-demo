# Format In CI Demo

Using dotnet format to check coding style in GitLab CI Pipelines.

See [my demo project in Github](https://github.com/twandylue/dotnet-formatter-in-GitHooks) for more details about `dotnet format x coding style x GitHooks`.
